import os

# Information
__license__ = 'GNU GPL v3'
__release__ = False
__author__ = __maintainer__ = 'Stanislav Arnaudov'
__email__ = 'stanislav_ts@abv.bg'

# Setup constants
CMDIR = os.path.dirname(__file__)
CONFDIR = os.path.expanduser('~/.config/code_manager')
